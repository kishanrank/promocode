<?php

use Illuminate\Support\Facades\Route;

Route::name('admin.')->group(function () {
    //Login Routes
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('postLogin');
    Route::get('/register', 'RegisterController@showRegisterForm')->name('register');
    Route::post('/register', 'RegisterController@register')->name('postRegister');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    //Forgot Password Routes
    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    //Reset Password Routes
    Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset', 'ResetPasswordController@reset')->name('password.update');
    // Email Verification Route(s)
    Route::get('/activate/{code}', 'ActivationController@activation')->name('activate.account');
    Route::get('/resend/code', 'ActivationController@resend')->name('resend.code');
});