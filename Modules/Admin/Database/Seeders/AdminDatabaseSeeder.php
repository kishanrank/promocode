<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\Admin;
use Modules\Admin\Entities\AdminActivationCode;
use Illuminate\Support\Str;

class AdminDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $admin = Admin::where('id', 1)->first();

        if (!$admin) {
            $admin = Admin::create([
                'name' => 'hyperlink',
                'email' => 'hyperlink@gmail.com',
                'password' => bcrypt('hyperlink'),
                'confirmed' => 1,
            ]);

            AdminActivationCode::create([
                'admin_id' => $admin->id,
                'code' => Str::random(128)
            ]);
        }
    }
}
