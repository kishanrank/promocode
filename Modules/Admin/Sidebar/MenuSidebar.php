<?php

namespace Modules\Admin\Sidebar;

use Modules\Core\Foundations\Menu;

class MenuSidebar
{
    protected $_menu;

    public function __construct()
    {
        $this->_menu = app(Menu::class);
        $this->initMenu();
    }

    public function initMenu()
    {
        $adminMenu = [
            "group" => "core::core.menu.access",
            "title" => "admin::admin.menu.admin",
            "route" => "admin.adminuser.index",
            "permission" => 'manage-admins',
            "icon" => "fas fa-users nav-icon",
            "active_actions" => [
                "admin.adminuser.index"
            ],
            'create' => 'admin.adminuser.create',
            "order" => 4,
        ];

        $this->_menu->addMenuItem($adminMenu);
    }
}
