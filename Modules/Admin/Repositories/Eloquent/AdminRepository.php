<?php

namespace Modules\Admin\Repositories\Eloquent;

use Exception;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\Admin;
use Illuminate\Support\Str;
use Modules\Admin\Repositories\AdminRepositoryInterface;
use Yajra\DataTables\Facades\DataTables;

class AdminRepository implements AdminRepositoryInterface
{
    public function all()
    {
        $data = $this->getGridData();
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('created_at', function ($admin) {
                return date('d-m-Y H:i:s', strtotime($admin->created_at));
            })
            ->editColumn('confirmed', function ($admin) {
                return $admin->confirmed_for_grid;
            })
            ->addColumn('action', function ($admin) {
                return $admin->action_buttons;
            })
            ->rawColumns(['action', 'confirmed'])
            ->make(true);
    }

    protected function getGridData()
    {
        return Admin::latest()->get();
    }

    public function store($request)
    {
        $userData = $request->all();

        $admin = $this->saveAdminData($userData);

        $admin->adminActivationCode()->create([
            'code' => Str::random(128)
        ]);
        return $admin;
    }

    protected function saveAdminData($userData)
    {
        $userData['password'] = bcrypt($userData['password']);
        return Admin::create($userData);
    }

    public function find($id)
    {
        return Admin::findOrFail($id);
    }

    public function destroy($id)
    {
        $currentUserId = $this->getCurrentLoggedInUser();
        $admin = Admin::findOrFail($id);

        if ($admin->id == 1) {
            throw new Exception('You can not delete Master admin account.');
        }

        if ($admin->id == $currentUserId) {
            throw new Exception('You can not delete your own account.');
        }

        if ($admin->delete()) {
            return true;
        }
        return false;
    }

    public function update($id, $request)
    {
        $admin = $this->find($id);
        $userData = $request->all();

        $admin->update($userData);

        return $admin;
    }

    public function getCurrentLoggedInUser()
    {
        return Auth::guard('admin')->user()->id;
    }
}
