<?php

namespace Modules\Admin\Repositories;

interface AdminRepositoryInterface
{
	public function all();

	public function store($request);

	public function find($id);

	public function destroy($id);

	public function update($id, $request);
}
