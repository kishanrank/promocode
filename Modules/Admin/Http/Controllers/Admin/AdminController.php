<?php

namespace Modules\Admin\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Modules\Admin\Entities\Admin;
use Modules\Admin\Events\Admin\ActivationCodeEvent;
use Modules\Admin\Http\Requests\StoreAdminRequest;
use Modules\Admin\Http\Requests\UpdateAdminRequest;
use Modules\Admin\Repositories\AdminRepositoryInterface;
use Modules\Core\Http\Controllers\Admin\CoreController;

class AdminController extends CoreController
{
    public $admin;

    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        $this->admin = $adminRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try {
            return view('admin::admin.index');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }

    /**
     *  return dynamic response for jquery detatables
     */
    public function get(Request $request)
    {
        try {
            if ($request->ajax()) {
                return $this->admin->all();
            }
            return $this->errorMessageResponse(['message' => trans('core::core.messages.something_wrong')]);
        } catch (\Throwable $e) {
            return $this->errorMessageResponse(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        try {
            return view('admin::admin.create');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.adminuser.index', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StoreAdminRequest $request)
    {
        try {
            $admin = $this->admin->store($request);
            if ($admin->id) {
                return $this->successRedirect('admin.adminuser.index', 'Admin user created successfully.');
            }
            return $this->errorRedirect('admin.adminuser.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $admin = $this->admin->find($id);
            return view('admin::admin.edit', compact('admin'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.adminuser.index', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateAdminRequest $request, $id)
    {
        try {
            $admin = $this->admin->update($id, $request);
            if ($admin) {
                return $this->successRedirect('admin.adminuser.index', 'Admin account updated successfully.');
            }
            return $this->errorRedirect('admin.adminuser.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.adminuser.index', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $admin = $this->admin->destroy($id);
            return $this->successRedirect('admin.adminuser.index', 'Admin user deleted successfully.');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.adminuser.index', $e->getMessage());
        }
    }

    public function profile()
    {
        try {
            $admin = Auth::guard('admin')->user();
            return view('admin::admin.account.profile', compact('admin'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }

    public function updatePassword(Request $request)
    {
        if ($request->ajax()) {
            $error = Validator::make($request->all(), $this->validatePasswordRule());

            if ($error->fails()) {
                return $this->errorMessageResponse(['errors' => $error->errors()->all()], 422);
            }

            $admin = Admin::findOrFail(Auth::guard('admin')->user()->id);
            $currentPassword = $this->getCurrentPassword();

            if (Hash::check($request->current_password, $currentPassword)) {
                $admin->password = bcrypt($request->password);
                if ($admin->save()) {
                    return $this->successMessageResponse(['message' => 'Password updated successfully.'], 200);
                }
            }
            return $this->errorMessageResponse(['message' => 'Your current password is not matching.'], 404);
        }
    }

    public function updateProfile(Request $request)
    {
        try {
            $adminUser = Admin::find(Auth::guard('admin')->user()->id);
            $adminUser->name = $request->name;
            $adminUser->email = $request->email;
            $adminUser->save();
            return $this->successRedirect('admin.adminuser.profile', 'Admin user updated successfully.');
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }

    public function getCurrentPassword()
    {
        return Auth::guard('admin')->user()->password;
    }

    public function validatePasswordRule()
    {
        return [
            'current_password' => 'required',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',
        ];
    }

    public function resend(Request $request)
    {
        try {
            $admin = Admin::whereEmail($request->email)->firstOrFail();
            if ($admin->isAdminActivated()) {
                return $this->successRedirect('admin.dashboard.index');
            }
            if (!$admin->adminActivationCode) {
                $admin->adminActivationCode()->create([
                    'code' => Str::random(128)
                ]);
                return view('theme::layouts.admin.errors.404');
            }
            event(new ActivationCodeEvent($admin));
            return $this->backWithSuccess('Email verification link resend successfully.');
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }
}
