<?php

namespace Modules\Admin\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Notification;
use Modules\Admin\Entities\Admin;
use Illuminate\Support\Str;
use Modules\Admin\Entities\AdminActivationCode;
use Modules\Admin\Events\Admin\ActivationCodeEvent;
use Modules\Admin\Notifications\Admin\AdminVerified;

class ActivationController extends Controller
{
    public function activation($code)
    {
        $code = AdminActivationCode::whereCode($code)->get()->first();
        if (!$code) {
            return view('theme::layouts.admin.errors.404');
        }
        $code->admin()->update([
            'confirmed' => true,
            'email_verified_at' => now()
        ]);

        $admin = $code->admin; //due to model property belongsTo we have admin data 
        $code->update([
            'code' => Str::random(128)
        ]);

        Notification::send($admin, new AdminVerified($admin->name));
        $notification = array(
            'message' => 'Your account is now activated successfully. Please Log-in.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.login')->with($notification);
    }

    public function resend(Request $request)
    {
        $admin = Admin::whereEmail($request->email)->firstOrFail();
        if ($admin->isAdminActivated()) {
            return redirect(route('admin.dashboard.index'));
        }
        if (!$admin->adminActivationCode) {
            $admin->adminActivationCode()->create([
                'code' => Str::random(128)
            ]);
            return view('theme::layouts.admin.errors.404');
        }
        event(new ActivationCodeEvent($admin));
        $notification = array(
            'message' => 'Email verification link resend successfully.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.login')->with($notification);
    }

}
