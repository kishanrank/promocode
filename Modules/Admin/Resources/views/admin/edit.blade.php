@extends('theme::layouts.admin.master')

@section('title')
{{ trans('admin::admin.titles.edit_admin') }}
@endsection

@section('extra-css')
@stop

@section('content-header')
<!-- Content Header (Page header) -->
<div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ trans('admin::admin.titles.edit_admin') }}</h1>
        </div>
        <div class="col-sm-6">
            <div class="float-right">
                {!! getBackButton('admin.adminuser.index')!!}
            </div>
        </div>
    </div>
</div>
<!-- /.content-header -->
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            {{ Form::model($admin, ['route' => ['admin.adminuser.update', $admin], 'class' => "form-horizontal", 'method' => 'put', 'id' => 'edit-adminuser'])}}
            <div class="card card-info card-outline">
                <div class="card-header with-border">
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        {{ Form::label('name', trans('admin::admin.form.label.name'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::text('name', null, ['class' => 'form-control box-size', 'id' => 'name', 'autocomplete' => 'off', 'placeholder' => trans('admin::admin.form.placeholder.name'), 'required' => 'required']) }}
                            @error('name')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ Form::label('email', trans('admin::admin.form.label.email'), ['class' => 'col-md-2 control-label label-right required']) }}

                        <div class="col-md-10">
                            {{ Form::email('email', null, ['class' => 'form-control box-size', 'id' => 'name', 'autocomplete' => 'off', 'placeholder' => trans('admin::admin.form.placeholder.email'), 'required' => 'required']) }}
                            @error('email')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="edit-form-btn">
                        {{ link_to_route('admin.adminuser.index', trans('core::core.buttons.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(trans('core::core.buttons.update'), ['class' => 'btn btn-primary btn-md']) }}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            {{ Form::close()}}
        </div>
    </div>
</div>
@stop

@section('after-js')
<script>
    $(document).ready(function() {
        displayPermission();
    });
</script>
@stop