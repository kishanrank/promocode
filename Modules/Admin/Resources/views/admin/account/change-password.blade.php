<div id="updateAdminPasswordModal" class="modal fade" role="dialog">
    <div class="modal-dialog" >
        <div class="modal-content" style="background: #fff">
            <div class="modal-header">
                <h4 class="modal-title">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="update_admin_password_form" class="form-horizontal">
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-md-4">Current Password :<sup class="text-danger">*</sup> </label>
                        <input type="password" name="current_password" id="current_password" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-6">New Password : <sup class="text-danger">*</sup></label>
                        <input type="password" name="password" id="password" class="form-control" min="8" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-6">Confirm Password : <sup class="text-danger">*</sup></label>
                        <input type="password" name="password_confirmation" id="password-confirm" class="form-control" min="8" required/>
                    </div>
            </div>
            <br />
            <div class="form-group text-center">
                <input type="submit" name="action_button" id="action_button" class="btn btn-primary" value="Update Password" />
            </div>
            </form>
        </div>
    </div>
</div>