<?php

namespace Modules\Core\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller;

class CoreController extends Controller
{
    public function successMessageResponse(array $data, $code = 200)
    {
        return response()->json(['type' => 'success', 'content' => $data, 'code' => $code], $code);
    }

    public function errorMessageResponse(array $data, $code = 400)
    {
        return response()->json(['type' => 'error', 'content' => $data, 'code' => $code], $code);
    }

    public function successRedirect($route, $message = null)
    {
        return redirect()->route($route)->with('success', $message);
    }

    public function redirectWithParameter($route, $parameter, $message = null)
    {
        return redirect()->route($route, $parameter)->with('success', $message);
    }

    public function errorRedirect($route, $message = null)
    {
        return redirect()->route($route)->with('error', $message);
    }

    public function backWithError($message)
    {
        return back()->with('error', $message)->withInput();
    }

    public function backWithSuccess($message)
    {
        return back()->with('success', $message);
    }
}
