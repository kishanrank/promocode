<div class="col-md-6 col-lg-6 col-sm-6 col-lg-offset-3">
    @if (Session::has('success'))
        <div class="bg-success">
            <p>{{ Session::get('success') }}</p>
        </div>
    @endif

    @if (Session::has('error'))
        <div class="bg-danger">
            <p>{{ Session::get('error') }}</p>
        </div>
    @endif

    @if (Session::has('warning'))
        <div class="bg-warning">
            {{ Session::get('warning') }}
        </div>
    @endif

    @if (Session::has('info'))
        <div class="bg-info">
            {{ Session::get('info') }}
        </div>
    @endif
</div>
<style type="text/css">
    .callout .callout-success {
        border-left-color: #28a745;
    }

    .callout .callout-danger {
        border-left-color: red;
    }

</style>
