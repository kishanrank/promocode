<!-- Navbar -->
<!-- <nav class="main-header navbar navbar-expand navbar-white navbar-light"> -->
<nav class="main-header navbar navbar-expand navbar-white">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="javascript:void(0);"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        </li>
        <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle admin-profile-right" data-toggle="dropdown">
                <span class="d-none d-md-inline">
                <img src="https://picsum.photos/50" alt="Admin Logo" class="img-circle elevation-2" style="opacity: .8">
                    DD Admin
                </span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right admin-right-menu">
                <!-- User image -->
                <li class="user-header bg-primary">
                    <img src="" alt="Admin Logo" class="brand-image img-circle" style="opacity: .8">
                    <p>
                        DD Admin
                    - Web Developer
                    <small>Member since {{ \Carbon\Carbon::parse($admin['created_at'])->diffForHumans()}}</small>
                    </p>
                </li>
                <li class="user-footer">
                    <a href="{{ route('admin.adminuser.profile') }}" class="btn btn-default btn-flat"><i class="fa fa-user">&nbsp;Profile</i></a>
                    <a class="btn btn-default btn-flat float-right" href="{{ route('admin.logout') }}"><i class="fas fa-sign-out-alt">&nbsp;Logout</i></a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
<!-- /.navbar -->