@extends('theme::layouts.frontend.master')

@section('content')
    <div class="col-md-6 col-lg-6 col-sm-6 col-lg-offset-3">
        <h4>Promocode Applied Successfully.</h4>
        <h3>Promocode : {{ $redeemedPromocode->promocode }} </h3>
        <h3>Discount Amount : {{ $redeemedPromocode->discount_amount }}</h3>
        <a href="{{ route('frontend.promocode.index') }}">Go to Home</a>
    </div>
@endsection
