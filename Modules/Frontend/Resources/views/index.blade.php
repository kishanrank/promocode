@extends('theme::layouts.frontend.master')

@section('content')
    <form method="post" action="{{ route('frontend.promocode.apply') }}">
        {{ csrf_field() }}

        <div class="col-md-6 col-lg-6 col-sm-6 col-lg-offset-3">
            <div class="form-group">
                <label class="col-form-label" for="user_id">User Id / username</label>
                {{ Form::text('user_id', null, ['class' => 'form-control','id' => 'user_id','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.user_id'),'required' => 'required']) }}
                @error('user_id')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label class="col-form-label" for="birthdate">Birthdate</label>
                {{ Form::date('birthdate', \Carbon\Carbon::now(), ['class' => 'form-control select2 required']) }}
                @error('birthdate')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label class="col-form-label" for="birthdate">Gender</label>
                {{ Form::select('gender', ['' => 'Select Gender', 1 => 'Male', 2 => 'Female', 3 => 'Other'], 'all', ['class' => 'form-control select2 required']) }}
                @error('gender')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label class="col-form-label" for="birthdate">Order Amount</label>
                {{ Form::number('order_amount', null, ['class' => 'form-control box-size','id' => 'order_amount','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.order_amount'),'required' => 'required']) }}
                @error('order_amount')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label class="col-form-label" for="birthdate">Promocode</label>
                {{ Form::text('promocode', null, ['class' => 'form-control box-size','id' => 'promocode','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.promocode'),'required' => 'required']) }}
                @error('promocode')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>


            <button type="Submit" class="btn btn-success btn-block">Apply Promocode</button><br><br>
        </div>
    </form>
@endsection
