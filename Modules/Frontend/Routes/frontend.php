<?php

use Illuminate\Support\Facades\Route;

Route::prefix('promocode')->group(function () {
    Route::get('/apply', [
        'as' => 'frontend.promocode.index',
        'uses' => 'IndexController@index',
    ]);

    Route::post('/apply', [
        'as' => 'frontend.promocode.apply',
        'uses' => 'IndexController@applyPromo',
    ]);
});
