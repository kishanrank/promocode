<?php

namespace Modules\Frontend\Http\Controllers;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Frontend\Http\Requests\ApplyPromoRequest;
use Modules\Promocode\Entities\Promocode;
use Modules\Promocode\Entities\RedeemedPromocode;

class IndexController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try {
            return view('frontend::index');
        } catch (\Throwable $th) {
            return view('theme::layouts.frontend.errors.404');
        }
    }

    public function applyPromo(ApplyPromoRequest $request)
    {
        try {
            $validatedData = $request->validated();

            $promocode = Promocode::wherePromocode($request->get('promocode'))->first();
            $redeemedPromocode = new RedeemedPromocode();
            $currentDate = strtotime(date('Y-m-d'));

            if (!$promocode || !$promocode->id) {
                throw new Exception('Invalid Promo code');
            }

            if ($request->get('gender') != $promocode->gender) {
                throw new Exception('This promocode is not available for you.');
            }

            if ($currentDate < strtotime($promocode->start_date)) {
                throw new Exception('Promocode is expired or not available.');
            }

            if ($currentDate > strtotime($promocode->end_date)) {
                throw new Exception('Promocode is expired');
            }

            $totalRedeemtionTillNow = $redeemedPromocode->getTotalPromocodeUsageCount($promocode->promocode);
            if ($totalRedeemtionTillNow >= $promocode->total_promocode_limit) {
                throw new Exception('This promocode is out of stock.');
            }

            $userRedeemtionTillNow = $redeemedPromocode->getPromocodeUsageCountByUser($request->get('user_id'), $promocode->promocode);
            if ($userRedeemtionTillNow >= $promocode->per_user_limit) {
                throw new Exception("You have already redeemed this promocode $promocode->per_user_limit times.");
            }

            $discountPercent = $promocode->percentage;

            $birthDate = $request->get('birthdate');
            $isBirthDate = (strtotime(date('Y-m-d')) == strtotime($birthDate)) ? true : false;
            if ($isBirthDate) {
                $discountPercent += Promocode::BIRTHDAY_DISCOUNT_PERCENT;
            }

            $orderAmount = $request->get('order_amount');
            $discountValue = ($orderAmount * $discountPercent) / 100;

            if ($promocode->max_discount_amount < $discountValue) {
                $discountValue = $promocode->max_discount_amount;
            }

            $validatedData['discount_percentage'] = $discountPercent;
            $validatedData['discount_amount'] = $discountValue;

            $redeemedPromocode = RedeemedPromocode::create($validatedData);

            $promocode->is_used = 1;
            $promocode->save();

            return view('frontend::success', compact('redeemedPromocode'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }
}
