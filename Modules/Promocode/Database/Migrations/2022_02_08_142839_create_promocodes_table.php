<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Promocode\Entities\Promocode;

class CreatePromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $promocode = new Promocode();
        Schema::create($promocode->getTable(), function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('promocode');
            $table->decimal('percentage');
            $table->decimal('max_discount_amount')->comment('Max. Discount Amount');
            $table->integer('total_promocode_limit')->default(10)->comment('Total number of promo usage');
            $table->integer('per_user_limit')->default(1)->comment('Num. of times same user can use');
            $table->integer('gender')->comment("1=>Male, 2=Female, 3=Other");
            $table->dateTime('start_date')->comment('Promocode usage start date');
            $table->dateTime('end_date')->comment('promocode expiry date');
            $table->boolean('is_used')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $promocode = new Promocode();
        Schema::dropIfExists($promocode->getTable());
    }
}
