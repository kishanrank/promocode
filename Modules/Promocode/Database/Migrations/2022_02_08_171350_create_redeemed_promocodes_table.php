<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Promocode\Entities\RedeemedPromocode;

class CreateRedeemedPromocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $redeemedPromocode = new RedeemedPromocode();
        Schema::create($redeemedPromocode->getTable(), function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->dateTime('birthdate');
            $table->smallInteger('gender');
            $table->decimal('order_amount');
            $table->string('promocode');
            $table->decimal('discount_percentage');
            $table->decimal('discount_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $redeemedPromocode = new RedeemedPromocode();
        Schema::dropIfExists($redeemedPromocode->getTable());
    }
}
