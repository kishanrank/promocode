<?php

namespace Modules\Promocode\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePromocodeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'promocode' => 'required',
            'number_of_promocode' => 'required|numeric',
            'percentage' => 'required|max:99',
            'max_discount_amount' => 'required|numeric',
            'total_promocode_limit' => 'required|numeric',
            'per_user_limit' => 'required|numeric|lt:total_promocode_limit',
            'gender' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
