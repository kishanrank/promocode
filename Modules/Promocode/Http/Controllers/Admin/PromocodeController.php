<?php

namespace Modules\Promocode\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Promocode\Entities\Promocode;
use Modules\Promocode\Http\Requests\StorePromocodeRequest;
use Modules\Promocode\Http\Requests\UpdatePromocodeRequest;
use Yajra\DataTables\Facades\DataTables;

class PromocodeController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try {
            return view('promocode::admin.promocode.index');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }

    /**
     *  return dynamic response for jquery detatables
     */
    public function get(Request $request)
    {
        try {
            if ($request->ajax()) {
                $promocodes = Promocode::latest()->get();
                return DataTables::of($promocodes)
                    ->addIndexColumn()
                    ->addColumn('action', function ($promocode) {
                        return $promocode->action_buttons;
                    })
                    ->editColumn('gender', function ($promocode) {
                        return $promocode->gender_text;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
            return $this->errorMessageResponse(['message' => trans('core::core.messages.something_wrong')]);
        } catch (\Throwable $e) {
            return $this->errorMessageResponse(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('promocode::admin.promocode.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StorePromocodeRequest $request)
    {
        try {
            $validatedData = $request->validated();

            $promocode = new Promocode();
            $finalData = array();
            $lastPromoId = $promocode->getLastPromoId();
            $numberOfPromocode = $request->get('number_of_promocode');
            $originalPromo = $request->get('promocode');
            $genderOptions = $promocode->getGenderOptions();
            unset($validatedData['number_of_promocode']);

            if ($numberOfPromocode && $numberOfPromocode > 0) {
                for ($promoQty = 1; $promoQty <= $numberOfPromocode; $promoQty++) {
                    $genderString =  isset($genderOptions[$validatedData['gender']]) ? $genderOptions[$validatedData['gender']] : 'M';
                    $genderChar = substr($genderString, 0, 1);
                    $newPromocode = $validatedData['promocode'] . $lastPromoId . $genderChar;
                    $newPromocode = str_replace(' ', '', $newPromocode);
                    $newPromocode = strtoupper($newPromocode);
                    $validatedData['promocode'] = $newPromocode;
                    $finalData[] = $validatedData;
                    $validatedData['promocode'] = $originalPromo;
                    $lastPromoId++;
                }
                Promocode::insert($finalData);
            }

            return $this->successRedirect('admin.promocodes.index', 'Promocodes created successfully.');
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        try {
            $promocode = Promocode::findOrFail($id);
            return view('promocode::admin.promocode.edit', compact('promocode'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.promocodes.index', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdatePromocodeRequest $request, $id)
    {
        try {
            $promocode = Promocode::findOrFail($id);
            $promocode->update($request->validated());
            return $this->successRedirect('admin.promocodes.index', 'Promocodes updated successfully.');
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $promocode = Promocode::findOrFail($id);
            $records = $promocode->delete();
            return $this->successRedirect('admin.promocodes.index', 'Promocodes deleted successfully.');
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }
}
