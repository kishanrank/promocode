<?php

namespace Modules\Promocode\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Promocode\Entities\RedeemedPromocode;
use Yajra\DataTables\Facades\DataTables;

class RedeemedPromocodeController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try {
            return view('promocode::admin.promocode.redeemed.index');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }

    /**
     *  return dynamic response for jquery detatables
     */
    public function get(Request $request)
    {
        try {
            if ($request->ajax()) {
                $redeemedPromocodes = RedeemedPromocode::latest()->get();
                return DataTables::of($redeemedPromocodes)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($admin) {
                        return date('d-m-Y H:i:s', strtotime($admin->created_at));
                    })
                    ->editColumn('gender', function ($redeemedPromocode) {
                        return $redeemedPromocode->gender_text;
                    })
                    ->make(true);
            }
            return $this->errorMessageResponse(['message' => trans('core::core.messages.something_wrong')]);
        } catch (\Throwable $e) {
            return $this->errorMessageResponse(['message' => $e->getMessage()], $e->getCode());
        }
    }
}
