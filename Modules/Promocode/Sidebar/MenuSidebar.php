<?php

namespace Modules\Promocode\Sidebar;

use Modules\Core\Foundations\Menu;

class MenuSidebar
{
    protected $_menu;

    public function __construct()
    {
        $this->_menu = app(Menu::class);
        $this->initMenu();
    }

    public function initMenu()
    {
        $menuItems = [
            "group" => "core::core.menu.single.promocode",
            "title" => "promocode::promocode.titles.manage_promocode",
            "route" => "admin.promocodes.index",
            "icon" => "fas fa-circle nav-icon",
            "active_actions" => [
                "admin.promocodes.index",
                "admin.promocodes.create"
            ],
            "order" => 5,
            'create' => 'admin.promocodes.create',
        ];

        $menuItem2 = [
            "group" => "core::core.menu.single.redeemed-promocode",
            "title" => "promocode::promocode.titles.manage_redeemed_promocode",
            "route" => "admin.redeemed.promocodes.index",
            "icon" => "fas fa-circle nav-icon",
            "active_actions" => [
                "admin.redeemed.promocodes.index",
            ],
            "order" => 6,
        ];
        $this->_menu->addMenuItem($menuItems);
        $this->_menu->addMenuItem($menuItem2);
    }
}
