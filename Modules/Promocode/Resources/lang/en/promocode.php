<?php

return [
    'titles' => [
        'manage_promocode'  => 'Manage Promocodes',
        'create_promocode'  => 'Create Promocode',
        'edit_promocode'    => 'Edit Promocode',
        'manage_redeemed_promocode' => 'Redeemed Promocode',
    ],
    'menu' => [
        'promocode' => 'Promocodes',
    ],
    'buttons' => [
        'cancel'    => 'Cancel',
        'create'    => 'Create New',
        'save'      => 'Save',
        'update'    => 'Update',
        'delete'    => 'Delete',
        'back'      => 'Back'
    ],
    'grid' => [
        'header' => [
            'no'                => 'No,',
            'name'              => 'Name',
            'promocode'         => 'Promocode',
            'percentage'        => 'Percentage',
            'max_discount_limit' => 'Max Discount Amount',
            'gender'            => 'Gender',
            'start_date'        => 'Start Date',
            'end_date'          => 'End Date',
            'created_at'        => 'Created At',
            'redeemed_at'       => 'Redeemed At',
            'action'            => 'Actions',
            'user_id'           => 'User Id',
            'birthdate'         => 'Birthdate',
            'order_amount'      => 'Order Amount',
            'discount_percentage' => 'Discount Percentage',
            'discount_amount'   => 'Discount Amount'
        ],
    ],
    'form' => [
        'label' => [
            'name'                  => 'Name : ',
            'promocode'             => 'Promocode : ',
            'percentage'            => 'Percentage : ',
            'max_discount_amount'   => 'Max Discount Amount : ',
            'number_of_promocode'   => 'Number of Promocode : ',
            'total_promocode_limit' => 'Total Promo Usage Limit : ',
            'per_user_limit'        => 'Per User Promo Limit : ',
            'gender'                => 'Gender : ',
            'start_date'            => 'Start Date : ',
            'end_date'              => 'End Date : ',
        ],
        'input' => [],
        'placeholder' => [
            'name'                  => 'Enter Name',
            'promocode'             => 'Enter Promocode',
            'percentage'            => 'Enter Percentage',
            'max_discount_amount'   => 'Enter Max Discount Amount',
            'number_of_promocode'   => 'Enter Number of Promocode',
            'total_promocode_limit' => 'Enter Total Promo Usage Limit',
            'per_user_limit'        => 'Enter Per User Promo Limit',
            'gender'                => 'Enter Gender',
            'start_date'            => 'Enter Start Date',
            'end_date'              => 'Enter End Date',
            'order_amount'          => 'Enter Order Amount',
            'user_id'               => 'Enter user id or name'
        ],
    ],
];
