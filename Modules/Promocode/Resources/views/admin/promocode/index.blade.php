@extends('theme::layouts.admin.master')

@section('title')
    {{ trans('promocode::promocode.titles.manage_promocode') }}
@endsection

@section('extra-css')
    @include('theme::asset.admin.css.datatable')
@stop

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ trans('promocode::promocode.titles.manage_promocode') }}</h1>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-info btn-sm float-right" href="{{ route('admin.promocodes.create') }}">Add New</a>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info card-outline">
                    <div class="card-body">
                        <div class="table-responsive data-table-wrapper">
                            <table id="promocodes-table" class="table table-condensed table-hover table-bordered"
                                width="100%">
                                <thead class="transparent-bg">
                                    <tr>
                                        <th>{{ trans('promocode::promocode.grid.header.no') }}</th>
                                        <th>{{ trans('promocode::promocode.grid.header.name') }}</th>
                                        <th>{{ trans('promocode::promocode.grid.header.promocode') }}</th>
                                        <th>{{ trans('promocode::promocode.grid.header.percentage') }}</th>
                                        <th>{{ trans('promocode::promocode.grid.header.max_discount_limit') }}</th>
                                        <th>{{ trans('promocode::promocode.grid.header.gender') }}</th>
                                        <th>{{ trans('promocode::promocode.grid.header.start_date') }}</th>
                                        <th>{{ trans('promocode::promocode.grid.header.end_date') }}</th>
                                        <th>{{ trans('promocode::promocode.grid.header.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    @include('theme::layouts.admin.modals.confirm')
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('after-js')
    @include('theme::asset.admin.js.datatable')
    <script>
        $(function() {
            var dataTable = $('#promocodes-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{{ route('admin.promocodes.get') }}',
                    type: 'post'
                },
                lengthMenu: [20, 30, 50, 100, 200],
                pageLength: 20,
                columns: [{
                        data: 'DT_RowIndex'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'promocode'
                    },
                    {
                        data: 'percentage'
                    },
                    {
                        data: 'max_discount_amount'
                    },
                    {
                        data: 'gender'
                    },
                    {
                        data: 'start_date'
                    },
                    {
                        data: 'end_date'
                    },
                    {
                        data: 'action',
                        searchable: false,
                        sortable: false
                    },
                ],
                searchDelay: 500,
            });
        });
    </script>
@stop
