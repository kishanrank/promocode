@extends('theme::layouts.admin.master')

@section('title')
    {{ trans('promocode::promocode.titles.create_promocode') }}
@endsection

@section('extra-css')
@stop

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ trans('promocode::promocode.titles.create_promocode') }}</h1>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    {!! getBackButton('admin.promocodes.index') !!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-header -->
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                {{ Form::open(['route' => 'admin.promocodes.store','class' => 'form-horizontal','method' => 'post','id' => 'create-promocode']) }}
                <div class="card card-info card-outline">
                    <div class="card-header with-border">
                    </div>

                    <div class="card-body">
                        <div class="form-group row">
                            {{ Form::label('name', trans('promocode::promocode.form.label.name'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::text('name', null, ['class' => 'form-control box-size','id' => 'name','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.name'),'required' => 'required']) }}
                                @error('name')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('promocode', trans('promocode::promocode.form.label.promocode'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::text('promocode', null, ['class' => 'form-control box-size','id' => 'promocode','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.promocode'),'required' => 'required']) }}
                                @error('promocode')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('number_of_promocode', trans('promocode::promocode.form.label.number_of_promocode'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::number('number_of_promocode', null, ['class' => 'form-control box-size','id' => 'number_of_promocode','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.number_of_promocode'),'required' => 'required']) }}
                                @error('number_of_promocode')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('percentage', trans('promocode::promocode.form.label.percentage'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::text('percentage', null, ['class' => 'form-control box-size','id' => 'percentage','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.percentage'),'required' => 'required']) }}
                                @error('percentage')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('max_discount_amount', trans('promocode::promocode.form.label.max_discount_amount'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::number('max_discount_amount', null, ['class' => 'form-control box-size','id' => 'max_discount_amount','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.max_discount_amount'),'required' => 'required']) }}
                                @error('max_discount_amount')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('total_promocode_limit', trans('promocode::promocode.form.label.total_promocode_limit'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::number('total_promocode_limit', null, ['class' => 'form-control box-size','id' => 'total_promocode_limit','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.total_promocode_limit'),'required' => 'required']) }}
                                @error('total_promocode_limit')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('per_user_limit', trans('promocode::promocode.form.label.per_user_limit'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::number('per_user_limit', null, ['class' => 'form-control box-size','id' => 'per_user_limit','autocomplete' => 'off','placeholder' => trans('promocode::promocode.form.placeholder.per_user_limit'),'required' => 'required']) }}
                                @error('per_user_limit')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('gender', trans('promocode::promocode.form.label.gender'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::select('gender', ['' => 'Select Gender', 1 => 'Male', 2 => 'Female', 3 => 'Other'], 'all', ['class' => 'form-control select2 box-size required']) }}
                                @error('gender')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('start_date', trans('promocode::promocode.form.label.start_date'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::date('start_date', \Carbon\Carbon::now(), ['class' => 'form-control select2 box-size required']) }}
                                @error('start_date')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::label('end_date', trans('promocode::promocode.form.label.end_date'), ['class' => 'col-md-2 control-label label-right required']) }}

                            <div class="col-md-10">
                                {{ Form::date('end_date', '', ['class' => 'form-control select2 box-size required']) }}
                                @error('end_date')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="edit-form-btn">
                            {{ link_to_route('admin.promocodes.index',trans('core::core.buttons.cancel'),[],['class' => 'btn btn-danger btn-md']) }}
                            {{ Form::submit(trans('core::core.buttons.save'), ['class' => 'btn btn-primary btn-md']) }}
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('after-js')
@stop
