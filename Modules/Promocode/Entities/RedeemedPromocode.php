<?php

namespace Modules\Promocode\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Traits\BaseModelTrait;
use Modules\Promocode\Entities\Traits\Admin\Attribute\RedeemedPromocodeAttribute;

class RedeemedPromocode extends Model
{
    use BaseModelTrait;
    use RedeemedPromocodeAttribute;

    public $table = 'redeemed_promocodes';

    protected $fillable = ['user_id', 'birthdate', 'gender', 'order_amount', 'promocode', 'discount_percentage', 'discount_amount'];

    public function getPromocodeUsageCountByUser($userId, $promocode)
    {
        return self::whereUserId($userId)->wherePromocode($promocode)->count();
    }

    public function getTotalPromocodeUsageCount($promocode)
    {
        return self::wherePromocode($promocode)->count();
    }
}
