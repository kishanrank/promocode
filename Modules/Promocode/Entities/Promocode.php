<?php

namespace Modules\Promocode\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Core\Entities\Traits\BaseModelTrait;
use Modules\Promocode\Entities\Traits\Admin\Attribute\PromocodeAttribute;

class Promocode extends Model
{
    use BaseModelTrait;
    use PromocodeAttribute;

    const BIRTHDAY_DISCOUNT_PERCENT = 10;

    public $table = 'promocodes';

    protected $fillable = ['name', 'promocode', 'percentage', 'max_discount_amount', 'total_promocode_limit', 'per_user_limit', 'gender', 'start_date', 'end_date', 'is_used'];

    public function getLastPromoId()
    {
        $lastPromo = self::orderBy('id', 'desc')->first();
        return ($lastPromo) ? $lastPromo->id + 1 : 1;
    }

    public function getGenderOptions()
    {
        return [
            1 => 'Male',
            2 => 'Female',
            3 => 'Other'
        ];
    }
}
