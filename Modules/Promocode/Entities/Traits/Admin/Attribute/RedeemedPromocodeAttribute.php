<?php

namespace Modules\Promocode\Entities\Traits\Admin\Attribute;

use Modules\Promocode\Entities\Promocode;

trait RedeemedPromocodeAttribute
{
    public function getGenderTextAttribute()
    {
        $promocode = new Promocode();
        $genderOptions = $promocode->getGenderOptions();

        if ($genderOptions && isset($genderOptions[$this->gender])) {
            return $genderOptions[$this->gender];
        }
        return 'Male';
    }
}
