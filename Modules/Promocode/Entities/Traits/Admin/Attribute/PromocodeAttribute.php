<?php

namespace Modules\Promocode\Entities\Traits\Admin\Attribute;

trait PromocodeAttribute
{
    public function getActionButtonsAttribute()
    {
        $data =  '<div class="btn-group action-btn">';
        $isDisable = $this->is_used;
        $eMessage = ($isDisable) ? 'You can not edit this Promocode as it is already used.' : null;
        $dMessage = ($isDisable) ? 'You can not delete this Promocode as it is already used.' : null;
        $data .= $this->getEditButtonAttribute('admin.promocodes.edit', $isDisable, $eMessage);
        $data .= $this->getDeleteButtonAttribute('admin.promocodes.destroy', $isDisable, $dMessage);
        $data .= '</div>';
        return $data;
    }

    public function getGenderTextAttribute()
    {
        $genderOptions = $this->getGenderOptions();

        if ($genderOptions && isset($genderOptions[$this->gender])) {
            return $genderOptions[$this->gender];
        }
        return 'Male';
    }
}
