<?php

use Illuminate\Support\Facades\Route;

Route::prefix('promocodes')->group(function () {
    Route::get('/', [
        'as' => 'admin.promocodes.index',
        'uses' => 'PromocodeController@index',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/get', [
        'as' => 'admin.promocodes.get',
        'uses' => 'PromocodeController@get',
        'middleware' => 'auth:admin'
    ]);

    Route::get('/create', [
        'as' => 'admin.promocodes.create',
        'uses' => 'PromocodeController@create',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/store', [
        'as' => 'admin.promocodes.store',
        'uses' => 'PromocodeController@store',
        'middleware' => 'auth:admin'
    ]);

    Route::get('/edit/{id}', [
        'as' => 'admin.promocodes.edit',
        'uses' => 'PromocodeController@edit',
        'middleware' => 'auth:admin'
    ]);

    Route::put('/update/{id}', [
        'as' => 'admin.promocodes.update',
        'uses' => 'PromocodeController@update',
        'middleware' => 'auth:admin'
    ]);

    Route::get('/delete/{id}', [
        'as' => 'admin.promocodes.destroy',
        'uses' => 'PromocodeController@destroy',
        'middleware' => 'auth:admin'
    ]);
});


Route::prefix('redeemed-promocodes')->group(function () {
    Route::get('/', [
        'as' => 'admin.redeemed.promocodes.index',
        'uses' => 'RedeemedPromocodeController@index',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/get', [
        'as' => 'admin.redeemed.promocodes.get',
        'uses' => 'RedeemedPromocodeController@get',
        'middleware' => 'auth:admin'
    ]);
});
